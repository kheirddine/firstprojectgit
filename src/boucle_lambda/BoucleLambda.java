package boucle_lambda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoucleLambda {
	public static void main(String[] args) {
		Map<String, Integer> items = new HashMap<>();
		items.put("kheir", 1);
		items.put("amine", 2);
		items.put("safae", 3);
		items.put("salah", 4);
		System.out.println("---- items ----");
		for (Map.Entry<String, Integer> entry : items.entrySet()) {
			System.out.println(" Numero = " + entry.getValue() + " ,name = " + entry.getKey());

		}
		System.out.println("-------------------------------------------------------------");
		items.forEach((k, v) -> System.out.println(" Numero = " + v + " ,name = " + k));

		System.out.println("-------------------------------------------------------------");
		List<String> items1 = new ArrayList<String>();
		items1.add("E");
		items1.add("C");
		items1.add("K");
		items1.add("A");
		items1.add("S");
		System.out.println("---- items1 ----");
		//lambda
		System.out.println("--------------------------- lambda ----------------------------------");
		items1.forEach(s->System.out.println(s));
		System.out.println("-------------------------------------------------------------");
		//methodes references
		System.out.println("--------------------------- methodes references ----------------------------------");
		items1.forEach(System.out::println);
		
	}

}
